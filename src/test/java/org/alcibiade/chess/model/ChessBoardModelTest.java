package org.alcibiade.chess.model;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChessBoardModelTest {

    @Test
    public void testBoardModel() {
        ChessBoardModel model1 = new ChessBoardModel();
        ChessBoardModel model2 = new ChessBoardModel();

        model1.setInitialPosition();
        model2.setPosition(model1);

        Assertions.assertThat(model1).isEqualTo(model2);
        Assertions.assertThat(model1.hashCode()).isEqualTo(model2.hashCode());

        model2.movePiece(new ChessBoardCoord("e2"), new ChessBoardCoord("e4"));
        Assertions.assertThat(model1).isNotEqualTo(model2);
        Assertions.assertThat(model1.hashCode()).isNotEqualTo(model2.hashCode());
    }

    @Test
    public void testAsciiOutput() {
        ChessBoardModel model1 = new ChessBoardModel();
        model1.setInitialPosition();
        Assertions.assertThat(model1.toString()).isEqualTo(
                "WHITE KQkq\n" +
                        "r n b q k b n r \n" +
                        "p p p p p p p p \n" +
                        ". . . . . . . . \n" +
                        ". . . . . . . . \n" +
                        ". . . . . . . . \n" +
                        ". . . . . . . . \n" +
                        "P P P P P P P P \n" +
                        "R N B Q K B N R \n"
        );
    }
}
